#include <iostream>

int faktorial(int n) {
    if (n == 0 || n == 1) {
        return 1;
    } else {
        return n * faktorial(n - 1);
    }
}

void kombinasi(int n, int r) {
    if (n >= r && r >= 0) {
        int hasil = faktorial(n) / (faktorial(r) * faktorial(n - r));
        std::cout << "C(" << n << ", " << r << ") = " << hasil << std::endl;
    } else {
        std::cout << "Input tidak valid." << std::endl;
    }
}

void permutasi(int n, int r) {
    if (n >= r && r >= 0) {
        int hasil = faktorial(n) / faktorial(n - r);
        std::cout << "P(" << n << ", " << r << ") = " << hasil << std::endl;
    } else {
        std::cout << "Input tidak valid." << std::endl;
    }
}

int main() {
    int n, r;

    std::cout << "Masukkan nilai n: ";
    std::cin >> n;
    std::cout << "Masukkan nilai r: ";
    std::cin >> r;

    std::cout << "\nKombinasi:\n";
    kombinasi(n, r);

    std::cout << "\nPermutasi:\n";
    permutasi(n, r);

    return 0;
}
