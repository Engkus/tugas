#include <iostream>
#include <stdlib.h>

using namespace std;

int main()
{
  system("clear");

    int N = 0;
    int A = 1; // Number of rows

    cout << "Masukkan baris : ";
    cin >> N;
    cout << "Masukan kolom : ";
    cin >> A;

    for (int i = 0; i <= N; i++) {
        for (int j = 1; j <= A; j++) {
            cout << "*";
        }
        cout << endl;
    } 

  return 0;
}
