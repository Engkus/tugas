#include <iostream>

using namespace std;

int main(){
    int gajipokok,gajibersih,tunjangan,potonganiuran;
    string  nama,status;
    char golongan;

    gajipokok = 350000;

    cout << "Nama karyawan            : "; cin >> nama;
    cout << "Golongan(A/B)            : "; cin >> golongan;
    cout << "Status(Nikah/Belum)      : "; cin >> status;

    if (golongan == 'B'){
      gajipokok = 200000;
    }
    cout << "Gaji pokok               : Rp. " << gajipokok << endl;
    
    if (golongan == 'A'){
      if (status == "Nikah"){
        tunjangan = 50000;
      }
      if (status == "Belum"){
        tunjangan = 25000;
      }
    }
    
    if (golongan == 'B'){
      if (status == "Nikah"){
        tunjangan = 70000;
      }
      if (status == "Belum"){
        tunjangan = 65000;
      }
    }
    cout << "Tunjangan                : Rp. " << tunjangan << endl;
    
    if (gajipokok <= 300000){
      potonganiuran = (gajipokok + tunjangan) * 0.05;
    }
      potonganiuran = (gajipokok + tunjangan) * 0.10;
    cout << "Potongan iuran           : Rp. " << potonganiuran << endl;
    
    gajibersih = gajipokok + tunjangan - potonganiuran;
    cout << "Gaji bersih              : Rp. " << gajibersih << endl;
    
    return 0;

}
